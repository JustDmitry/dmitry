﻿namespace test2
{
    partial class LoadForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.b_Yes = new System.Windows.Forms.Button();
            this.b_No = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(79, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(134, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Upload last search?";
            // 
            // b_Yes
            // 
            this.b_Yes.Location = new System.Drawing.Point(33, 64);
            this.b_Yes.Name = "b_Yes";
            this.b_Yes.Size = new System.Drawing.Size(100, 25);
            this.b_Yes.TabIndex = 1;
            this.b_Yes.Text = "Yes";
            this.b_Yes.UseVisualStyleBackColor = true;
            this.b_Yes.Click += new System.EventHandler(this.B_Yes_Click);
            // 
            // b_No
            // 
            this.b_No.Location = new System.Drawing.Point(159, 64);
            this.b_No.Name = "b_No";
            this.b_No.Size = new System.Drawing.Size(100, 25);
            this.b_No.TabIndex = 2;
            this.b_No.Text = "No";
            this.b_No.UseVisualStyleBackColor = true;
            this.b_No.Click += new System.EventHandler(this.B_No_Click);
            // 
            // LoadForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(292, 101);
            this.Controls.Add(this.b_No);
            this.Controls.Add(this.b_Yes);
            this.Controls.Add(this.label1);
            this.Name = "LoadForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Last search";
            this.Load += new System.EventHandler(this.LoadForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button b_Yes;
        private System.Windows.Forms.Button b_No;
    }
}