﻿namespace test2
{
    partial class MainForm
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.tb_Search = new System.Windows.Forms.TextBox();
            this.b_Select = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.tb_FileName = new System.Windows.Forms.TextBox();
            this.tb_Template = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.b_Search = new System.Windows.Forms.Button();
            this.lb_Result = new System.Windows.Forms.ListBox();
            this.b_Load = new System.Windows.Forms.Button();
            this.b_Clear = new System.Windows.Forms.Button();
            this.b_ClearAll = new System.Windows.Forms.Button();
            this.cb_Sub = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(163, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Please select directory to search:";
            // 
            // tb_Search
            // 
            this.tb_Search.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tb_Search.Location = new System.Drawing.Point(16, 29);
            this.tb_Search.Name = "tb_Search";
            this.tb_Search.Size = new System.Drawing.Size(379, 20);
            this.tb_Search.TabIndex = 1;
            // 
            // b_Select
            // 
            this.b_Select.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.b_Select.Location = new System.Drawing.Point(401, 27);
            this.b_Select.Name = "b_Select";
            this.b_Select.Size = new System.Drawing.Size(75, 23);
            this.b_Select.TabIndex = 2;
            this.b_Select.Text = "Select...";
            this.b_Select.UseVisualStyleBackColor = true;
            this.b_Select.Click += new System.EventHandler(this.B_Select_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 56);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(80, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Enter file name:";
            // 
            // tb_FileName
            // 
            this.tb_FileName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tb_FileName.Location = new System.Drawing.Point(16, 72);
            this.tb_FileName.Name = "tb_FileName";
            this.tb_FileName.Size = new System.Drawing.Size(199, 20);
            this.tb_FileName.TabIndex = 4;
            // 
            // tb_Template
            // 
            this.tb_Template.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.tb_Template.Location = new System.Drawing.Point(225, 72);
            this.tb_Template.Name = "tb_Template";
            this.tb_Template.Size = new System.Drawing.Size(170, 20);
            this.tb_Template.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(222, 56);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(69, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "File template:";
            // 
            // b_Search
            // 
            this.b_Search.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.b_Search.Location = new System.Drawing.Point(401, 70);
            this.b_Search.Name = "b_Search";
            this.b_Search.Size = new System.Drawing.Size(75, 23);
            this.b_Search.TabIndex = 7;
            this.b_Search.Text = "Search";
            this.b_Search.UseVisualStyleBackColor = true;
            this.b_Search.Click += new System.EventHandler(this.B_Search_Click);
            // 
            // lb_Result
            // 
            this.lb_Result.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lb_Result.FormattingEnabled = true;
            this.lb_Result.Location = new System.Drawing.Point(16, 108);
            this.lb_Result.Name = "lb_Result";
            this.lb_Result.Size = new System.Drawing.Size(379, 316);
            this.lb_Result.TabIndex = 8;
            // 
            // b_Load
            // 
            this.b_Load.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.b_Load.Location = new System.Drawing.Point(401, 343);
            this.b_Load.Name = "b_Load";
            this.b_Load.Size = new System.Drawing.Size(75, 23);
            this.b_Load.TabIndex = 9;
            this.b_Load.Text = "Load...";
            this.b_Load.UseVisualStyleBackColor = true;
            this.b_Load.Click += new System.EventHandler(this.B_Load_Click);
            // 
            // b_Clear
            // 
            this.b_Clear.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.b_Clear.Location = new System.Drawing.Point(401, 372);
            this.b_Clear.Name = "b_Clear";
            this.b_Clear.Size = new System.Drawing.Size(75, 23);
            this.b_Clear.TabIndex = 10;
            this.b_Clear.Text = "Clear";
            this.b_Clear.UseVisualStyleBackColor = true;
            this.b_Clear.Click += new System.EventHandler(this.B_Clear_Click);
            // 
            // b_ClearAll
            // 
            this.b_ClearAll.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.b_ClearAll.Location = new System.Drawing.Point(401, 401);
            this.b_ClearAll.Name = "b_ClearAll";
            this.b_ClearAll.Size = new System.Drawing.Size(75, 23);
            this.b_ClearAll.TabIndex = 11;
            this.b_ClearAll.Text = "Clear all";
            this.b_ClearAll.UseVisualStyleBackColor = true;
            this.b_ClearAll.Click += new System.EventHandler(this.B_ClearAll_Click);
            // 
            // cb_Sub
            // 
            this.cb_Sub.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cb_Sub.AutoSize = true;
            this.cb_Sub.Location = new System.Drawing.Point(401, 108);
            this.cb_Sub.Name = "cb_Sub";
            this.cb_Sub.Size = new System.Drawing.Size(76, 17);
            this.cb_Sub.TabIndex = 12;
            this.cb_Sub.Text = "Subfolders";
            this.cb_Sub.UseVisualStyleBackColor = true;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(489, 439);
            this.Controls.Add(this.cb_Sub);
            this.Controls.Add(this.b_ClearAll);
            this.Controls.Add(this.b_Clear);
            this.Controls.Add(this.b_Load);
            this.Controls.Add(this.lb_Result);
            this.Controls.Add(this.b_Search);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.tb_Template);
            this.Controls.Add(this.tb_FileName);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.b_Select);
            this.Controls.Add(this.tb_Search);
            this.Controls.Add(this.label1);
            this.Name = "MainForm";
            this.Text = "Search file";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tb_Search;
        private System.Windows.Forms.Button b_Select;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tb_FileName;
        private System.Windows.Forms.TextBox tb_Template;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button b_Search;
        private System.Windows.Forms.ListBox lb_Result;
        private System.Windows.Forms.Button b_Load;
        private System.Windows.Forms.Button b_Clear;
        private System.Windows.Forms.Button b_ClearAll;
        private System.Windows.Forms.CheckBox cb_Sub;
    }
}

