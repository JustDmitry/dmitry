﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;


namespace test2
{
    public partial class MainForm : Form
    {
        SearchProperties sp = new SearchProperties();

        public MainForm()
        {
            InitializeComponent();
        }

        private void B_Select_Click(object sender, EventArgs e)
        {
            SelectPath();
        }

        private void B_Search_Click(object sender, EventArgs e)
        {
            SaveProps();
            B_Clear_Click(sender, e);

            try
            {
                DirectoryInfo dir = new DirectoryInfo(tb_Search.Text);
                string fileName = tb_FileName.Text;

                if (cb_Sub.Checked == true)
                {
                    SearchFiles(dir, fileName, true);
                }
                else
                {
                    SearchFiles(dir, fileName, false);
                }
            }
            catch(ArgumentException ex)
            {
                lb_Result.Items.Add(ex.Message);
            }
            catch (DirectoryNotFoundException ex)
            {
                lb_Result.Items.Add(ex.Message);
            }

        }
        
        private void B_Load_Click(object sender, EventArgs e)
        {
            LoadForm lf = new LoadForm();
            lf.Owner = this;

            if (lf.ShowDialog() == DialogResult.OK && File.Exists(Environment.CurrentDirectory + "PathSearch.fsp"))
            {
                tb_Search.Text = File.ReadAllText(Environment.CurrentDirectory + "PathSearch.fsp");
                tb_FileName.Text = File.ReadAllText(Environment.CurrentDirectory + "FileName.fsp");
                tb_Template.Text = File.ReadAllText(Environment.CurrentDirectory + "PatternFile.fsp");
            }
        }

        private void B_Clear_Click(object sender, EventArgs e)
        {
            lb_Result.Items.Clear();
        }

        private void B_ClearAll_Click(object sender, EventArgs e)
        {
            tb_Search.Text = "";
            tb_FileName.Text = "";
            tb_Template.Text = "";
            lb_Result.Items.Clear();
        }

        private void SelectPath()
        {
            FolderBrowserDialog fbd = new FolderBrowserDialog();
            fbd.ShowNewFolderButton = false;
            fbd.Description = "\nSelect path to search:";

            if (fbd.ShowDialog() == DialogResult.OK)
            {
                tb_Search.Text = fbd.SelectedPath;
            }
        }

        private void SaveProps()
        {
            sp.Path = tb_Search.Text;
            sp.FileName = tb_FileName.Text;
            sp.PatternFile = tb_Template.Text;

            File.WriteAllText(Environment.CurrentDirectory + "PathSearch.fsp", sp.Path);
            File.WriteAllText(Environment.CurrentDirectory + "FileName.fsp", sp.FileName);
            File.WriteAllText(Environment.CurrentDirectory + "PatternFile.fsp", sp.PatternFile);
        }

        private void SearchFiles(DirectoryInfo dir, string fileName, bool subfolders)
        {
            if(subfolders)
            {
                try
                {
                    foreach (FileInfo file in dir.GetFiles(fileName + "*" + tb_Template.Text, SearchOption.AllDirectories))
                    {
                        lb_Result.Items.Add(file.FullName);
                    }
                }
                catch (UnauthorizedAccessException ex)
                {
                    lb_Result.Items.Add(ex.Message);
                }
                catch (DirectoryNotFoundException ex)
                {
                    lb_Result.Items.Add(ex.Message);
                }
                catch (Exception ex)
                {
                    lb_Result.Items.Add(ex.Message);
                }
            }
            try
            {
                foreach (FileInfo file in dir.GetFiles(fileName + "*" + tb_Template.Text))
                {
                    lb_Result.Items.Add(file.FullName);
                }
            }
            catch (UnauthorizedAccessException ex)
            {
                lb_Result.Items.Add(ex.Message);
            }
            catch (DirectoryNotFoundException ex)
            {
                lb_Result.Items.Add(ex.Message);
            }
            catch (Exception ex)
            {
                lb_Result.Items.Add(ex.Message);
            }
        }
    }
}



